package com.hgg.data;

import org.apache.commons.math3.ml.clustering.Clusterable;

public class TwitterClusterPoint implements Clusterable
{
	public TwitterMessageSkeletal message;


	public TwitterClusterPoint(TwitterMessageSkeletal message)
	{
		this.message = message;
	}


	public double[] getPoint()
	{
		return new double[]{message.geo.coordinates[0], message.geo.coordinates[1]};
	}
}
