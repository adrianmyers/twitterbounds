package com.hgg.data;

public class TwitterMessageSkeletal
{
	public String id_str;
	public String text;
	public TwitterGeo geo;
	public TwitterGeo coordinates;
}
