package com.hgg.nio;

import com.hgg.config.AppConfig;
import com.hgg.entry.Router;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;


public class SocketServer
{
	private AsynchronousServerSocketChannel listener;
	private Router router;
	private CompletionHandler<AsynchronousSocketChannel,Void> acceptHandler;

	private Map<AsynchronousSocketChannel, Client> clientMap;
	private LinkedList<Client> clientPool;
	private LinkedList<ClientRequestBuilder> clientRequestBuilderPool;


	public SocketServer()
	{
		//
	}


	public Client AddClient(AsynchronousSocketChannel channel)
	{
		synchronized (clientMap)
		{
			if (clientMap.containsKey(channel))
			{
				System.out.println("******** DUPLICATE CLIENT CONNECT" );
				return clientMap.get(channel);
			}
		}
		Client client = clientPool.size()>0 ? clientPool.removeFirst() : new Client(this);
		client.Socket = channel;
		synchronized (clientMap)
		{
			clientMap.put(channel, client);
		}
		return client;
	}



	public void CloseClient(Client client)
	{
		try
		{
			client.Socket.close();
		} catch (IOException e) { e.printStackTrace(); }
		PoolClient(client);
	}




	public void PoolClient(Client client)
	{
		synchronized (clientMap)
		{
			if (clientMap.containsKey(client.Socket))
				clientMap.remove(client.Socket);
			clientPool.add(client);
		}
	}



	public void Init(Router router)
	{
		this.router = router;

		clientMap = new HashMap<AsynchronousSocketChannel, Client>();
		clientPool = new LinkedList<Client>();
		clientRequestBuilderPool = new LinkedList<ClientRequestBuilder>();

		try
		{
			listener = AsynchronousServerSocketChannel.open().bind(new InetSocketAddress(AppConfig.Data.Port));
			listener.accept(this, new SocketAcceptHandler());
		}
		catch (Exception e)
		{
			System.out.println("[SocketServer] ** ERROR: Unable to initialize socket server > "+e.toString());
		}
	}



	public ClientRequestBuilder GetNextClientRequestBuilder(Client client)
	{
		ClientRequestBuilder r = clientRequestBuilderPool.size()>0 ? clientRequestBuilderPool.removeFirst() : new ClientRequestBuilder();
		r.Init(client);
		return r;
	}
	public void PoolClientRequestBuilder(Client client)
	{
		ClientRequestBuilder r = client.RequestBuilder;
		client.RequestBuilder = null;
		r.Client = null;
		r.ReadBuffer.clear();
		clientRequestBuilderPool.add(r);
	}


	public void BroadcastWS(String message)
	{
		for(Client client : clientMap.values())
		{
			client.SendWS(message);
		}
	}


	private class SocketAcceptHandler implements CompletionHandler<AsynchronousSocketChannel, SocketServer>
	{

		public void completed(AsynchronousSocketChannel result, SocketServer server)
		{
			Client client = server.AddClient(result);

			// Start the client listen/read chain
			result.read(client.ReadBytes, client, new SocketReadHandler());

			// Immediately begin accepting the next new connection.
			listener.accept(server, this);
		}

		public void failed(Throwable exc, SocketServer attachment)
		{

		}
	}



	private class SocketReadHandler implements CompletionHandler<Integer, Client>
	{

		private byte GCode;
		private byte PCode;
		private byte UCode;
		private byte DCode;
		private byte BraceCode;
		private byte BracketCode;

		public SocketReadHandler()
		{
			try
			{
				byte[] b = ("GPUD{[").getBytes("UTF-8");
				GCode = b[0];
				PCode = b[1];
				UCode = b[2];
				DCode = b[3];
				BraceCode = b[4];
				BracketCode = b[5];
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		public void completed(Integer bytesRead, Client client)
		{
			// If this packet is a continuation of an older one, the client will still have its RequestBuilder.
			ClientRequestBuilder builder = null;
			if (client.RequestBuilder != null)
			{
				builder = client.RequestBuilder;
			}
			else
			{
				builder = GetNextClientRequestBuilder(client);
				builder.Type = DeterminteCommType(client.ReadBytes, 0);
				client.RequestBuilder = builder;
			}

			// We have to handle both messages spanning several packets, and packets containing several messages.
			// This is complicated somewhat by message type, because HTTP messages need some header parsing.

			// HTTP insight taken from https://www.jmarshall.com/easy/http/

			// We've read everything in this packet.
			// Let's loop over the bytes until they are all processed.
			int bytesProcessed = 0;
			while ( bytesProcessed < bytesRead )
			{
				int len = bytesRead - bytesProcessed;
				boolean isMoreToGo = false;
				boolean isMessageReady = false;

				boolean isContentUnknown = true;
				boolean hasBody = false;
				int contentLen = 0;
				boolean isHeaderDone = false;

				boolean isWSConnection = false;
				String wsKey;

				// If we have an existing builder that knew how many bytes to expect,
				//   then we can skip all the message scanning work, although we may have more bytes in the packet that need it later.
				if ( builder.ExpectedBytesRemaining > 0 )
				{
					len = Math.min(bytesRead, builder.ExpectedBytesRemaining);
				}
				else
				{
					// If it's an HTTP type message, we have to determine its total length.
					// The header ends in a CRLFCRLF sequence (13 10 13 10), and may also include content length if there is a body.
					if (builder.Type == ClientRequest.Types.HTTP)
					{
						boolean wasCRLF = false;

						for (int i = bytesProcessed; i <= bytesRead; i++)
						{
							// Until we find the end of the header (CRLFCRLF), we are looking for content length.
							// Content length always appears on a line like (CRLF)Content-Length: xxxCRLF
							// However, headers don't have case requirements (!).
							// This means we test every character after a CRLF to see if it is another CRLF to end the header,
							//   or the upper or lower case "C".
							// If it's a "C", we can look for the "ngth:" pattern at a specific index, again both cases.
							// I think (?) that's the only header field with that signature. So if it's there, we parse the string
							//   from the : to the CRLF as an int and call that content legnth.
							if (!isHeaderDone)
							{
								// Everything we are looking for happens after a CRLF
								if ( !wasCRLF )
								{
									if (client.ReadBytes.get(i) == 13 && client.ReadBytes.get(i + 1) == 10)
									{
										wasCRLF = true;
										i++; // Nudge i ahead to make the pattern easier.
										continue;
									}
								}
								else
								{
									// If we were following a CRLF and get another, it's the end of the header.
									// Maybe we never got content length, in which case we assume the header is all we need and bail out.
									// This is true for GET requests.
									if (client.ReadBytes.get(i) == 13 && client.ReadBytes.get(i + 1) == 10)
									{
										isHeaderDone = true;
										isMessageReady = true;
										if ( !hasBody )
										{
											len = i + 2; // End of the header
											builder.ExpectedBytesRemaining = len;


											// Do stuff if this is a WS connection attempt.
										}
										else
										{
											builder.ExpectedBytesRemaining = i + 2 + contentLen;
											len = Math.min(bytesRead, builder.ExpectedBytesRemaining); // Add content byte length if it was found.
										}
										break;
									}
									else
									{
										wasCRLF = false;

										if ( isContentUnknown )
										{
											// Content length test.
											// ASCII table: http://www.theasciicode.com.ar/american-standard-code-information-interchange/ascii-codes-table.png

											// I'm breaking this out to be explicit since debug/modify is nuts on the clever code version.
											// This is the "C" "c" test.
											if (client.ReadBytes.get(i) == 67 || client.ReadBytes.get(i) == 99 )
											{
												// Now we know where to look for "NGTH:" or "ngth:"
												int j = i+10;
												if (
												(client.ReadBytes.get(j) == 78 && client.ReadBytes.get(j+1) == 71 && client.ReadBytes.get(j+2) == 84 && client.ReadBytes.get(j+3) == 72 && client.ReadBytes.get(j+4) == 58 ) ||
												(client.ReadBytes.get(j) == 110 && client.ReadBytes.get(j+1) == 103 && client.ReadBytes.get(j+2) == 116 && client.ReadBytes.get(j+3) == 104 && client.ReadBytes.get(j+4) == 58 ))
												{
													// Now we need to scan to the expected CRLF
													// The largest value for content length we care about is 2^32, which is 9 digits.
													// We can make life a lot easier and be lazy and assume a max length of that plus a space before the CRLF.
													// So the furthest we probably need to look is 12 bytes out.
													// Also, we happily assume that there are actually bytes to treat as numbers. If the first thing we saw
													//   was CRLF, that would be bad.
													for( int k=j+5; k<j+18; k++ )
													{
														if (client.ReadBytes.get(k) == 13 && client.ReadBytes.get(k + 1) == 10)
														{
															String lengthStr = new String(client.ReadBytes.array(), j+5, k-(j+5));
															contentLen = Integer.parseInt(lengthStr.trim());
															isContentUnknown = false;
															hasBody = true;
															i = k+1;
															wasCRLF = true;
															break;
														}
													}

													// If isContentUnknown is still true here, then that loop is probably breaking on dirty data or some bad assumption.
													// This is a good place to start debugging future problems.

												}
											}




											// WS Connection test.
											// Uses pretty much the same logic, just looking for one very specific string by ASCII value.
											// We're looking for Sec-WebSocket-Key:, using "S" and "Key:"
											if (client.ReadBytes.get(i) == 83)
											{
												int j = i+14;
												if (
													client.ReadBytes.get(j) == 75 &&
													client.ReadBytes.get(j+1) == 101 &&
													client.ReadBytes.get(j+2) == 121 &&
													client.ReadBytes.get(j+3) == 58)
												{
													isWSConnection = true;
													builder.Type = ClientRequest.Types.WS;

													for( int k=j+4; k<j+64; k++ )
													{
														if (client.ReadBytes.get(k) == 13 && client.ReadBytes.get(k + 1) == 10)
														{
															wsKey = new String(client.ReadBytes.array(), j+5, k-(j+5));


															// In theory, there could be more bytes to read here.
															// But odds are good nothing followed the handshake on this socket, and we have what we need now.
															// In which case, rather than more scanning here, you could just barf up a retrn and bail out.
															HandleWSConnection(builder, client, wsKey);

															i = k+1;
															wasCRLF = true;
															break;
														}
													}
												}
											}


										}
									}
								}
							}
						} // http header byte loop
					}
					else if (builder.Type == ClientRequest.Types.String || builder.Type==ClientRequest.Types.JSON)
					{
						len = bytesRead - bytesProcessed;
						for (int i = bytesProcessed; i <= bytesRead; i++)
						{
							if (client.ReadBytes.get(i)==0)
							{
								len = i;
								break;
							}
						}
					}
					else if (builder.Type == ClientRequest.Types.Bytes)
					{
						builder.ExpectedBytesRemaining = client.ReadBytes.getInt(bytesProcessed);
						bytesProcessed += 4;
						client.ReadBytesOffset = bytesProcessed;
						len = Math.min(bytesRead-bytesProcessed, builder.ExpectedBytesRemaining);
					}
					else if (builder.Type == ClientRequest.Types.WS)
					{
						// https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers
					}
				}

				// If we know our message length is less than the number of bytes we've read,
				//   then this packet must include more than one message.
				if (len + bytesProcessed < bytesRead)
					isMoreToGo = true;

				// Update the Builder with the current read bytes (needs to know if it's final or not).
				builder.Read(len, isMoreToGo);

				// If we know we have a finished message, let's wrap it in a Request and send it off, and reset the Builder.
				if ( builder.ExpectedBytesRemaining == 0 )
				{
					ClientRequest r = new ClientRequest();
					r.Init(client, builder);
					router.QueueRequest(r);
					builder.Reset();
				}

				// Update the bytes processed tally
				bytesProcessed += len;

				// If we have more messages to process, keep the builder but determine the next message type,
				//   since it may be sneaky and change mid-packet if there is a break.
				// Otherwise, if we don't expect continuation, return this builder to the pool, and move on.
				if ( isMoreToGo )
					builder.Type = DeterminteCommType(client.ReadBytes, bytesProcessed);
				else if ( builder.ExpectedBytesRemaining == 0 )
					PoolClientRequestBuilder(client); // This is where you'd switch to detach only, and let the builder be the request for Router, to save an array copy.
			}

			// Continue the client listen/read chain
			client.Socket.read(client.ReadBytes, client, this);
		}



		private void HandleWSConnection(ClientRequestBuilder builder, Client client, String wsKey)
		{
			String magicKey = wsKey + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
			MessageDigest md = null;
			try
			{
				md = MessageDigest.getInstance("SHA-1");
				byte[] bytes = md.digest(magicKey.getBytes("UTF-8"));

				Base64.Encoder be = Base64.getEncoder();
				byte[] bytes64 = be.encode(bytes);
				String s = new String(bytes64, "UTF-8");

				String response = "HTTP/1.1 101 Switching Protocols\r\n";
				response += "Upgrade: websocket\r\n";
				response += "Connection: Upgrade\r\n";
				response += "Sec-WebSocket-Accept: " + s + "\r\n";
				response += "\r\n";

				System.out.println("\n");
				System.out.println("[Server] wsKey = " + wsKey);
				System.out.println("[Server] response = " + response);

				client.Send(response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}



		private ClientRequest.Types DeterminteCommType(ByteBuffer bytes, int position)
		{
			byte b = bytes.get(position);
			if (b==(byte)1)
				return ClientRequest.Types.Bytes;
			else if (b==GCode || b==PCode || b==UCode || b==DCode)
				return ClientRequest.Types.HTTP;
			else if (b==BraceCode || b==BracketCode)
				return ClientRequest.Types.JSON;
			else if (b==(byte)2)
				return ClientRequest.Types.String;
			return ClientRequest.Types.Undefined;
		}


		public void failed(Throwable exc, Client attachment)
		{

		}
	}

}
