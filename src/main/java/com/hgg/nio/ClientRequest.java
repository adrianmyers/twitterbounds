package com.hgg.nio;

import java.nio.ByteBuffer;

public class ClientRequest
{
	public Client Client;
	public byte[] Bytes;



	public enum Types
	{
		Undefined,
		HTTP,   // Begins with GET, POST, UPDATE, DELETE
		WS,     // Websocket, initial connection begins with GET, uses ridiculous frame format and XOR decoder ring bullshit thereafter.
		JSON,   // Begins with { [
		Bytes,  // Begins with 0x01
		String  // Begins with 0x02 or none of the above (dangerous catchall)
	}
	public Types Type;

	public enum SubTypes
	{
		Undefined,

		HTTPGet,
		HTTPPost,
		HTTPUpdate,
		HTTPDelete,

		WSConnect,
		WSData // Not sure if these are needed, basically the first packet will always be connect (special case) and the rest data.
	}
	public SubTypes SubType;



	public void Init(Client client, ClientRequestBuilder builder)
	{
		Client = client;
		Type = builder.Type;
		Bytes = builder.GetFinalBytes();
	}


}
