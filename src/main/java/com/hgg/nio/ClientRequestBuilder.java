package com.hgg.nio;

import java.nio.ByteBuffer;

public class ClientRequestBuilder
{
	// Clients have small 4K buffers that the sockets write directly into.
	// We ultimately want to have a large(ish) bytearray containing the contents of all packets in a Request for the
	//   app to process.
	// To build that Request array from the small read buffer chunks, we use a ClientRequestBuilder.
	// Because ClientRequestBuilder should theoretically be able to handle very large total buffers, or disk access
	//   if needed, it is a very expensive class, so its population is pooled separately from both Clients
	//   and active ClientRequests.

	// In general...
	// There is a Client with a 4K byte array for every current connected client.
	// There is one ClientRequest for every currently-processed request from all clients. These are created/destroyed as needed.
	// There is a pool of ClientRequestBuilders that never shrinks, but is only as large as the peak concurrent demand, and this can be improved.


	public Client Client;


	// This larger buffer is used to compose messages that exceed the packet capacity.
	// These can be dynamically allocated, or pooled, to avoid having a huge fixed allocation which
	//   will mostly go unused (which is what this static allocation will do!).
	public ByteBuffer ReadBuffer = ByteBuffer.allocate(1*1024*1024);

	// With multi-packet messages where you have a length, this can be very helpful.
	public int ExpectedBytesRemaining = 0;

	public ClientRequest.Types Type;



	public void Init(Client client)
	{
		Client = client;
	}



	public void Read(int len, boolean isMultiMessage)
	{
		System.out.println("<< " + len);

		if (len>0)
		{
			byte[] b = Client.ReadBytes.array();
			ReadBuffer.put(b, Client.ReadBytesOffset, len);

			if (isMultiMessage)
			{
				Client.ReadBytesOffset += len;
			} else
			{
				Client.ReadBytesOffset = 0;
				Client.ReadBytes.clear();
			}

			if (ExpectedBytesRemaining >0)
				ExpectedBytesRemaining = Math.max(ExpectedBytesRemaining - len, 0);
		}
	}


	public void Reset()
	{
		ReadBuffer.clear();
		ExpectedBytesRemaining = 0;
	}


	public byte[] GetFinalBytes()
	{
		ExpectedBytesRemaining = 0;
		int l = ReadBuffer.position();
		ReadBuffer.position(0);
		byte[] b = new byte[l];
		ReadBuffer.get(b, 0, l);
		ReadBuffer.clear();
		return b;
	}
}
