package com.hgg.nio;

import java.io.UnsupportedEncodingException;
import java.nio.*;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Client
{
	public SocketServer Server;
	public AsynchronousSocketChannel Socket;

	// This small buffer receives each packet's bytes.
	public ByteBuffer ReadBytes = ByteBuffer.allocate(4096);

	// This larger buffer handles easy string conversion.
	public ByteBuffer StringWriteBytes = ByteBuffer.allocate(1024*1024*1); // 1MB string space

	// If we have more than one message in the read bytes, we need to know where
	//   to start reading n+1 after processing n.
	public int ReadBytesOffset = 0;

	public ClientRequestBuilder RequestBuilder;


	// Not sure about this, HTTP doesn't use it and that's really the only place the disambiguation matters (vs. WebSocket)
	public enum Statuses
	{
		Connecting,
		Connected
	}
	public Statuses Status;




	public Client(SocketServer server)
	{
		Server = server;
	}


	public void Send(String message)
	{
		try
		{
			byte[] bytes = message.getBytes("UTF-8");
			StringWriteBytes.position(0);
			StringWriteBytes.put(bytes);
			StringWriteBytes.limit(bytes.length);
			StringWriteBytes.position(0);
			Future<Integer> f = Socket.write(StringWriteBytes);
			System.out.println(">> " + f.get());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void Send(byte[] bytes)
	{
		//
	}

	public void SendWS(String message)
	{
		//https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers

		// This format is a little insane.
		// For now, I'm assuming all comms to the client are STR and fit in one message.

		try
		{
			StringWriteBytes.position(0);
			byte[] payload = message.getBytes("UTF-8");


			// First we have a FIN bit, which is 0 for continuation messages and 1 for final messages. We always use one.
			// The three bits following that are always 0.
			// The four bits following those are the opcode, which for text/string messages is always 0x1
			byte b0 = (byte) ((1 << 7) | 1);
			StringWriteBytes.put(b0);

			// Then we have the mask bit, which for testing will be 0, and then 7 bits of payload length.
			// Length is involved:
			//   If len < 126, we just encode it directly in these 7 bits.
			//   If len is between 126 and 2^16, we set these bits to 126 and the next 2 bytes as the 16 bit length.
			//   If len > 2^16, we set these bits to 127 and the next 8 bytes as the 64-bit length.
			// I'm happily assuming that we'll never really send a 64KB string, so this only does the <126 test.
			byte b1 = (byte) 0;
			int len = payload.length;
			if (len < 126)
			{
				b1 |= (byte) len;
				StringWriteBytes.put(b1);
				StringWriteBytes.put(payload);
				StringWriteBytes.limit(len + 2);
			}
			else
			{
				b1 |= (byte)126;
				byte b2 = (byte)Math.floorDiv(len, 0xFF);
				byte b3 = (byte)(len % 0xFF);
				StringWriteBytes.put(b1);
				StringWriteBytes.put(b2);
				StringWriteBytes.put(b3);
				StringWriteBytes.put(payload);
				StringWriteBytes.limit(len + 4);
			}

			StringWriteBytes.position(0);
			Future<Integer> f = Socket.write(StringWriteBytes);
			System.out.println(">> " + f.get());

		} catch (Exception e)
		{
			e.printStackTrace();
		}

	}



}
