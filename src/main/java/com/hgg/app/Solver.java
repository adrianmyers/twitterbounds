package com.hgg.app;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hgg.data.TwitterClusterPoint;
import com.hgg.data.TwitterMessageSkeletal;
import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;


public class Solver
{
	private ArrayList<TwitterClusterPoint> points;



	public Solver()
	{
		//
	}



	public void Solve(List<TwitterMessageSkeletal> messages)
	{
		points = new ArrayList<TwitterClusterPoint>();
		for(TwitterMessageSkeletal message : messages)
			points.add(new TwitterClusterPoint(message));

		KMeansPlusPlusClusterer<TwitterClusterPoint> cl2 = new KMeansPlusPlusClusterer<TwitterClusterPoint>(2);
		KMeansPlusPlusClusterer<TwitterClusterPoint> cl3 = new KMeansPlusPlusClusterer<TwitterClusterPoint>(3);
		KMeansPlusPlusClusterer<TwitterClusterPoint> cl4 = new KMeansPlusPlusClusterer<TwitterClusterPoint>(4);

		List<CentroidCluster<TwitterClusterPoint>> cl2r = cl2.cluster(points);
		List<CentroidCluster<TwitterClusterPoint>> cl3r = cl3.cluster(points);
		List<CentroidCluster<TwitterClusterPoint>> cl4r = cl4.cluster(points);

		boolean useCl3 = !isTooCloseToCl2(cl2r, cl3r);
		boolean useCl4 = !isTooCloseToCl2(cl2r, cl4r);

		List<CentroidCluster<TwitterClusterPoint>> clusters = useCl4 ? cl4r : (useCl3 ? cl3r : cl2r);

		ArrayList<double[]> clusterBounds = new ArrayList<>();
		for(CentroidCluster cluster : clusters)
			clusterBounds.add(getBounds(cluster.getPoints()));

		int moo = 1;
	}



	private boolean isTooCloseToCl2(List<CentroidCluster<TwitterClusterPoint>> cl2r, List<CentroidCluster<TwitterClusterPoint>> test)
	{
		// Returns true if too many points in the supplied cluster are too close to either point in the default binary cluster.
		// "Too many" can be taken to mean k-1, and "too close" can be expressed as a % of the distance between centers in the binary case, here defaulting to 20%.
		// Also returns true if any cluster size is 1

		// Also returns true if k is the same!
		if (cl2r.size() == test.size()) return true;

		double[] p0 = cl2r.get(0).getCenter().getPoint();
		double[] p1 = cl2r.get(1).getCenter().getPoint();
		double dist = dist(p0, p1);

		int k = test.size();
		int hits = 0;
		for(CentroidCluster cluster : test)
		{
			if (cluster.getPoints().size() < 2)
				return true;

			double[] p = cluster.getCenter().getPoint();
			if ( dist(p, p0) < dist*0.2 || dist(p, p1) < dist*0.2 )
			{
				if (++hits == k-1)
					return true;
			}
		}
		return false;
	}



	private double dist(double[] p0, double[] p1)
	{
		double[] p = new double[]{p1[0]-p0[0], p1[1]-p0[1]};
		return Math.sqrt((p[0] * p[0]) + (p[1] * p[1]));
	}



	public double[] getBounds(List<TwitterClusterPoint> points)
	{
		//                              min lat,lon,   max lat,lon
		double[] bounds = new double[]{ 999.0,999.0, -999.0,-999.0 };

		double[] p = null;
		for(TwitterClusterPoint point : points)
		{
			p = point.getPoint();
			bounds[0] = Math.min(bounds[0], p[0]);
			bounds[1] = Math.min(bounds[1], p[1]);
			bounds[2] = Math.max(bounds[2], p[0]);
			bounds[3] = Math.max(bounds[3], p[1]);
		}

		return bounds;
	}




	public static void main(String[] args)
	{
		Solver solver = new Solver();

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));

		try
		{
			byte[] bytes = Files.readAllBytes(Paths.get("RecentMessages.json"));
			String json = new String(bytes, "UTF-8");
			TwitterMessageSkeletal[] _messages = mapper.readValue(json, TwitterMessageSkeletal[].class);
			ArrayList<TwitterMessageSkeletal> messages = new ArrayList<>();
			Collections.addAll(messages, _messages);
			solver.Solve(messages);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}



}
