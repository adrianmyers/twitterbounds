package com.hgg.app;

import com.hgg.config.AppConfig;
import com.hgg.entry.Router;
import com.hgg.entry.TwitterEventHandler;
import com.hgg.entry.TwitterReader;
import com.hgg.entry.TwitterStreamingClient;
import com.hgg.nio.SocketServer;

import java.security.MessageDigest;
import java.util.Base64;

public class Main
{
	private static SocketServer server;
	private static Router router;
	private static TwitterReader tReader;
	private static TwitterEventHandler tHandler;
	private static TwitterStreamingClient tClient;



	public static void main(String[] args)
	{
		// Twitter comms
		tReader = new TwitterReader();
		tReader.Init();

		tHandler = new TwitterEventHandler();
		tHandler.Init();

		tClient = new TwitterStreamingClient();
		tClient.Init(tReader.queue, tHandler.queue);




		// JS comms
//		router = new Router();
//		router.Init();
//
//		server = new SocketServer();
//		server.Init(router);




		// Background pulse on main thread if needed.
		while(true)
		{
			try
			{
//				server.BroadcastWS("HALLELUJIAH!");
				//System.out.println("[Main Pulse]");
				Thread.sleep(10000);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

}
