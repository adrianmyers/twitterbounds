package com.hgg.entry;

import com.twitter.hbc.core.event.Event;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


public class TwitterEventHandler implements Runnable
{
	public BlockingQueue<Event> queue = new LinkedBlockingQueue<Event>();



	public TwitterEventHandler()
	{
		//
	}


	public void Init()
	{
		Thread t = new Thread(this);
		t.start();
	}


	public void run()
	{
		System.out.println("[Events] Running...");
		try
		{

			while (true)
			{
				Event e = queue.take();
				System.out.println("[Events] !! " + e.toString() + " > " + e.getMessage());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


}
