package com.hgg.entry;

import com.hgg.config.AppConfig;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.Location;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.event.Event;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


public class TwitterStreamingClient implements Runnable
{

    Client hosebirdClient;

    BlockingQueue<String> msgQueue = new LinkedBlockingQueue<String>(100000);
    BlockingQueue<Event> eventQueue = new LinkedBlockingQueue<Event>(1000);




	public TwitterStreamingClient()
	{
		//
	}




	public void Init(BlockingQueue<String> messages, BlockingQueue<Event> events)
    {
        this.msgQueue = messages;
		this.eventQueue = events;

		Thread t = new Thread(this);
		t.start();
    }



    public void run()
	{
		Hosts hosts = new HttpHosts(Constants.STREAM_HOST);
		StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();

//		// Optional: set up some followings and track terms
//		List<Long> followings = Lists.newArrayList(1234L, 566788L);
//		List<String> terms = Lists.newArrayList("twitter", "api");
//		endpoint.followings(followings);
//		endpoint.trackTerms(terms);

		ArrayList<Location> locations = new ArrayList<Location>();
//		locations.add(new Location(
//			new Location.Coordinate(p.swLon, p.swLat),
//			new Location.Coordinate(p.neLon, p.neLat)));
		locations.add(new Location(
				new Location.Coordinate(122.1240234375, 29.53522956294847),
				new Location.Coordinate(152.8857421875, 55.37911044801047)));
		endpoint.locations(locations);

		//Authentication hosebirdAuth = new OAuth1(p.consumerKey, p.consumerSecret, p.authKey, p.authSecret);
		Authentication hosebirdAuth = new OAuth1(AppConfig.Data.consumer_key, AppConfig.Data.consumer_secret, AppConfig.Data.auth_key, AppConfig.Data.auth_secret);

		ClientBuilder builder = new ClientBuilder()
				.name("Hosebird-Client-01")                              // optional: mainly for the logs
				.hosts(hosts)
				.authentication(hosebirdAuth)
				.endpoint(endpoint)
				.processor(new StringDelimitedProcessor(msgQueue))
				.eventMessageQueue(eventQueue);                          // optional: use this if you want to process client events

		hosebirdClient = builder.build();


		System.out.println("[Twitter Client] Connecting...");
		hosebirdClient.connect();

		while(true)
		{
			//System.out.println("[Reader Pulse]");
			try
			{
				Thread.sleep(10000);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
    }


}
