package com.hgg.entry;

import Twitter.Message;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hgg.data.TwitterMessageSkeletal;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.file.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;


public class TwitterReader implements Runnable
{
	public BlockingQueue<String> queue = new LinkedBlockingQueue<String>();
	private ObjectMapper mapper;

	public ArrayList<TwitterMessageSkeletal> currentMessages;


	public TwitterReader()
	{
		//
	}


	public void Init()
	{
		mapper = new ObjectMapper();
		mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));

		currentMessages = new ArrayList<TwitterMessageSkeletal>(4096);

		Thread t = new Thread(this);
		t.start();
	}


	public void run()
	{
		System.out.println("[Reader] Running...");

		double lastBatchTS = System.currentTimeMillis();

		try
		{

			while (true)
			{
				String json = queue.take();
				TwitterMessageSkeletal m =  mapper.readValue(json, TwitterMessageSkeletal.class);

				// Only interested in tweets with geo
				if (m.geo!=null && m.geo.coordinates != null && m.geo.coordinates.length > 1)
				{
					System.out.println("[Reader] << " + json);

					currentMessages.add(m);

					// Every so often, chunk on disk.
					double now = System.currentTimeMillis();
					if (now - lastBatchTS > (1000 * 10 * 1))
					{
						System.out.println("\n[Reader] !! WRITING TO DISK\n");
						String bigUglyJson = mapper.writeValueAsString(currentMessages);

						Path path = Paths.get("RecentMessages.json");
						//Files.write(path, bigUglyJson.getBytes(Charset.forName("UTF-8")), Files.exists(path) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE_NEW);
						Files.write(path, bigUglyJson.getBytes(Charset.forName("UTF-8")), StandardOpenOption.CREATE);
						currentMessages.clear();

						lastBatchTS = now;
					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


}
