package com.hgg.entry;

import com.hgg.nio.ClientRequest;

import java.io.UnsupportedEncodingException;
import java.util.LinkedList;


public class Router implements Runnable
{
	private LinkedList<ClientRequest> requests = new LinkedList<ClientRequest>();
	private Object qLock = new Object();



	public Router()
	{
		//
	}


	public void Init()
	{
		Thread t = new Thread(this);
		t.start();
	}


	public void run()
	{
		try
		{
			ClientRequest r = null;
			while (true)
			{
				synchronized (qLock)
				{
					while (requests.isEmpty())
						qLock.wait();
					r = requests.removeFirst();
				}

				String s = null;
				try
				{
					s = new String(r.Bytes, "UTF-8");
				} catch (UnsupportedEncodingException e)
				{
					e.printStackTrace();
				}
				System.out.println("Type << " + r.Type.toString());
				System.out.println(s);

				if (r.Type == ClientRequest.Types.HTTP)
				{
					//
				}
				else if (r.Type == ClientRequest.Types.String)
				{
					//
				}
				else if (r.Type == ClientRequest.Types.JSON)
				{
					//
				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public void QueueRequest(ClientRequest r)
	{
		System.out.println("++ " + r.Bytes.length);
		synchronized (qLock)
		{
			requests.add(r);
			qLock.notify();
		}
	}


}
